package usbRelay;

import java.io.IOException;
import java.sql.*;


public class UsbRelayThread implements Runnable {

	// Current relay status:
	private static String status = "0000"; 

	
	// SQL connection variables:
	String url = "jdbc:mysql://localhost:3306/usbRelay";
	String user = "idanhahn";
	String password = "idan1980";
	
	public void run() {
		while (true){
			// check DB
			
			String newStatus = getDBstatus();
			
			if (!newStatus.equals(status)){
				status = newStatus;
				
				ProcessBuilder pb = new ProcessBuilder("res/relay.sh","4 turn " + status);
				try {
					pb.start();
					Thread.sleep(1000);
				} catch (IOException e) {
					e.printStackTrace();
					System.err.println("Failed to set the relays to status: " + status);
					System.exit(1);
				} catch (InterruptedException e) {
					System.err.println("Failed on sleep");
					System.exit(1);
				}
			}
		}
	}
	
	
	private String getDBstatus(){
		String DBstatus = new String();
		Connection 	con = null;
		Statement 	st 	= null;
		ResultSet 	rs 	= null;
		
		try {
			
			con = DriverManager.getConnection(url,user,password);
			st = con.createStatement();
			rs = st.executeQuery("SELECT * FROM relays");
		
			while (rs.next()){
				DBstatus = DBstatus.concat(rs.getString("status"));
			}
			
			
		} catch (SQLException e) {
			System.err.println("Failed to query data base");
			System.exit(1);
		} finally {
			try {
				if (con != null){
					con.close();
				}
				if (st != null){
					st.close();
				}
			} catch (SQLException e) {
				System.err.println("Failed on closing query");
				System.exit(1);
			}
		}
		return DBstatus;
	}
}
